fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## iOS
### ios register_app
```
fastlane ios register_app
```
register Your app ,with this lane
### ios upload_app_to_Testflight
```
fastlane ios upload_app_to_Testflight
```

### ios get_dev_certs
```
fastlane ios get_dev_certs
```
get dev certs ,with this lane
### ios sync_all_development
```
fastlane ios sync_all_development
```

### ios sync_device_info
```
fastlane ios sync_device_info
```

### ios _CI
```
fastlane ios _CI
```
Testing : CI
### ios release
```
fastlane ios release
```
Testing ,Archive, export and upload a build for a given environment. : CI/CD

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
