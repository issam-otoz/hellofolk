//
//  Helper.swift
//  HelloFolk
//
//  Created by Issam on 3/19/21.
//

import Foundation
func Print(_ items: Any..., separator: String = " ", terminator: String = "\n"){
    #if DEBUG
    print(items,separator,terminator)
    #endif
}
