//
//  UISettings.swift
//  HelloFolk
//
//  Created by Issam on 3/17/21.
//

import Foundation
import SwiftMessages

public class UISettingsClass {
    
    public static var errorMessageTheme: Theme = .info
    
    public static var defaultMessageTheme: Theme = .success
}
public typealias UISettings = UISettingsClass
