//
//  Theme.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift
import SwiftyJSON

class ThemeApp {
    
    static func Refrash(){
        ThemeApp.shared = ThemeApp()
        ThemeApp.Colors = Color()
        ThemeApp.Fonts = Font()
    }
    
    static var shared = ThemeApp()
    
    static var Colors = Color()

    static var Fonts = Font()
    
    func isLight() -> Bool{
        let u = User.getCurrentUser()
        return !(u.isDark ?? false)
    }
    
    func isArabic() -> Bool{
        let u = User.getCurrentUser()
        return (u.mobilelangue == LanguageName.Arabic.rawValue)
    }
    
    func fontSize() -> Double{
        
        let u = User.getCurrentUser()
        
        switch u.fontSizeId {
        case 0:
            return 12
        case 1:
            return 16
        case 2:
            return 18
        case 3:
            return 22
        default:
            return 18
        }
    }
    
    func getSettings() -> String?{
        return UserDefaults.standard.value(forKey: "settings") as? String
    }

    
    class Font{
        var regular8   = UIFont(name: Constants.PoppinsRegular, size: 8 )!
        var regular11  = UIFont(name: Constants.PoppinsRegular, size: 11)!
        var regular12  = UIFont(name: Constants.PoppinsRegular, size: 12)!
        var regular13  = UIFont(name: Constants.PoppinsRegular, size: 13)!
        var regular14  = UIFont(name: Constants.PoppinsRegular, size: 14)!
        var regular15  = UIFont(name: Constants.PoppinsRegular, size: 15)!
        
        var medium11   = UIFont(name: Constants.PoppinsMedium, size: 11)!
        var medium12   = UIFont(name: Constants.PoppinsMedium, size: 12)!
        var medium13   = UIFont(name: Constants.PoppinsMedium, size: 13)!
        var medium14   = UIFont(name: Constants.PoppinsMedium, size: 14)!
        var medium16   = UIFont(name: Constants.PoppinsMedium, size: 16)!
        var medium17   = UIFont(name: Constants.PoppinsMedium, size: 17)!
        var medium20   = UIFont(name: Constants.PoppinsMedium, size: 20)!
        
        
        var semiBold14 = UIFont(name: Constants.PoppinsSemiBold, size: 14)!
        var semiBold16 = UIFont(name: Constants.PoppinsSemiBold, size: 16)!
        var semiBold18 = UIFont(name: Constants.PoppinsSemiBold, size: 18)!
        var semiBold20 = UIFont(name: Constants.PoppinsSemiBold, size: 20)!
        
        var Bold = UIFont.init(name: Constants.CoconNextArabicBold, size: CGFloat(ThemeApp.shared.fontSize()))!
        var BoldL = UIFont.init(name: Constants.CoconNextArabicBold, size: CGFloat(ThemeApp.shared.fontSize()))!
        var BoldM = UIFont.init(name: Constants.CoconNextArabicBold, size: CGFloat(ThemeApp.shared.fontSize()) - 4)!
        var BoldS = UIFont.init(name: Constants.CoconNextArabicBold, size: CGFloat(ThemeApp.shared.fontSize()) - 8)!
        var Light = UIFont.init(name: Constants.CoconNextArabicLight, size: CGFloat(ThemeApp.shared.fontSize()))!
        var LightL = UIFont.init(name: Constants.CoconNextArabicLight, size: CGFloat(ThemeApp.shared.fontSize()) )!
        var LightM = UIFont.init(name: Constants.CoconNextArabicLight, size: CGFloat(ThemeApp.shared.fontSize()) - 4)!
        var LightS = UIFont.init(name: Constants.CoconNextArabicLight, size: CGFloat(ThemeApp.shared.fontSize()) - 8)!
        var RegularL = UIFont.init(name: Constants.CoconNextArabicLight, size: CGFloat(ThemeApp.shared.fontSize() + 2))!
        var Regular = UIFont.init(name: Constants.CoconNextArabicLight, size: CGFloat(ThemeApp.shared.fontSize()))!
        var RegularS = UIFont.init(name: Constants.CoconNextArabicLight, size: CGFloat(ThemeApp.shared.fontSize() - 2))!

    }
    
    
    static func setupStyle(){
        
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barTintColor = ThemeApp.Colors.background
        UINavigationBar.appearance().tintColor = ThemeApp.Colors.blue
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedString.Key.font : ThemeApp.Fonts.Bold.withSize(20),
            NSAttributedString.Key.foregroundColor : ThemeApp.Colors.blue
        ]
        UINavigationBar.appearance().barStyle = ThemeApp.shared.isLight() ? UIBarStyle.default : UIBarStyle.blackOpaque
        IQKeyboardManager.shared.keyboardAppearance = ThemeApp.shared.isLight() ? UIKeyboardAppearance.light : UIKeyboardAppearance.dark
        UINavigationBar.appearance().shadowImage = UIImage() //remove pesky 1 pixel line
    }
    
}



class Color{
    
    var primary       = UIColor.init("#4D777C")
    var dark          = UIColor.init("#1B2735")
    var secondary     = UIColor.init("#A0A0A0")
    var borders       = UIColor.init("#DCDCDC")
    var light         = UIColor.init("#F5F5F5")
    var white         = UIColor.init("#FFFFFF")
    var green         = UIColor.init("#00FF00")
    var radioBtnEmpty = UIColor.init("#C0C0C0")
    var title         = UIColor.init("#252B37")
    var b2b1bb        = UIColor.init("#B2B1BB")
    var darkPlaceholder = UIColor.rgba(red: 27, green: 39, blue: 53, alpha: 0.5)
    
    var navBar = UIColor.init("550096")
    var background = ThemeApp.shared.isLight() ? UIColor.init("FFFFFF") : UIColor.init("1A1A1A")
    var background_gray = ThemeApp.shared.isLight() ? UIColor.init("808080") : UIColor.init("E5E5E5")
    var font = ThemeApp.shared.isLight() ? UIColor.init("808080") : UIColor.white //gray font
    var font_blue = ThemeApp.shared.isLight() ? UIColor.init("253B66") : UIColor.white //blue font
    var gray = ThemeApp.shared.isLight() ? UIColor.init("253B66") : UIColor.white
    var red = UIColor.init("D73C2B")
    var blue = UIColor.init("00A0C6") // 00A0C6
    
    var card = ThemeApp.shared.isLight() ? UIColor.init("D8D8D8").withAlphaComponent(0.37) : UIColor.init("#333333").withAlphaComponent(0.30)
    
    var card2 = ThemeApp.shared.isLight() ? UIColor.white : UIColor.init("1D1D1D")
    var card3 = ThemeApp.shared.isLight() ? UIColor.white : UIColor.init("5B5B5B")
    
    var tfBackground = ThemeApp.shared.isLight() ? UIColor.white : UIColor.init("333333")
    
    var tabBarTint = ThemeApp.shared.isLight() ? UIColor.groupTableViewBackground : UIColor.init("333333")
    var tabBarIcons = ThemeApp.shared.isLight() ? UIColor.gray : UIColor.white
    
}
