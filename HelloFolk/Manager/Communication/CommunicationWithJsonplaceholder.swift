//
//  CommunicationWithJsonplaceholder.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import Then
import RxSwift

extension Communication{
    
    func getUsers() -> Observable<ApiResult<usersM, ApiErrorType>>{
        return Observable.create { [self] observer in
            if isMockData{
                let m = Communication.shared.loadJson(filename: "users", objectType: usersM.self)
                observer.on(.next(.init(value: m ?? [])))
                observer.onCompleted()
            }else{
                let url = API.users
                let rx = SendRequest( Url: url, method: .get, params: [:], objectType: usersM.self)
                rx.subscribe(onNext: { res in
                    observer.on(.next(res))
                  },onError: { (error) in
                    observer.on(.error(error))
                  },onCompleted: {
                    observer.onCompleted()
                  })
                  .disposed(by: disposeBag)
            }
            return Disposables.create()
        }

    }
    func getPosts()-> Observable<ApiResult<postsM, ApiErrorType>>{
        return Observable.create { [self] observer in
            if isMockData{
                let m = Communication.shared.loadJson(filename: "posts", objectType: postsM.self)
                observer.on(.next(.init(value: m ?? [])))
                observer.onCompleted()
            }else{
                let url = API.posts
                let rx = SendRequest(Url: url, method: .get, params: [:], objectType: postsM.self)
                rx
                  .subscribe(onNext: { res in
                    observer.on(.next(res))
                  },onError: { (error) in
                    observer.on(.error(error))
                  },onCompleted: {
                    observer.onCompleted()
                  })
                  .disposed(by: disposeBag)
            }
            return Disposables.create()
        }
        
//        switch res{
//        case .success(let data):
//            break
//        case .failure(let error):
//            break
//        }

    }
    func getCommentsPost(postId : Int) -> Observable<ApiResult<CommentsM, ApiErrorType>>{
        return Observable.create { [self] observer in
            if isMockData{
                var m = Communication.shared.loadJson(filename: "comments", objectType: CommentsM.self)
                m = m?.filter({ (res) -> Bool in
                    return (res.postID ?? -1) == postId
                })
                observer.on(.next(.init(value: m ?? [])))
                observer.onCompleted()
            }else{
                let url = API.commentsPost.replacingOccurrences(of: "{ID}", with: "\(postId)", options: .literal, range: nil)
                let rx = SendRequest(Url: url, method: .get, params: [:], objectType: CommentsM.self)
                rx
                  .subscribe(onNext: { res in
                    observer.on(.next(res))
                  },onError: { (error) in
                    observer.on(.error(error))
                  },onCompleted: {
                    observer.onCompleted()
                  })
                  .disposed(by: disposeBag)
            }
            return Disposables.create()
        }
    }
    
}
