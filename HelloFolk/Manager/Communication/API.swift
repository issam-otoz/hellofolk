//
//  API.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import Foundation
class API: NSObject {
    
    public static var api: String = "http://jsonplaceholder.typicode.com/";
    public static var posts: String = api + "posts";
    public static var users: String = api + "users";
    public static var commentsPost: String = api + "comments?postId={ID}";
}
