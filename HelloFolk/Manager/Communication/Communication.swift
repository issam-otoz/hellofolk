//
//  Communication.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import RxSwift
import RxCocoa

var notific = NotificationCenter.default
let _CommeToForground = "CommeToForground"
let _RequestErrorNotificationReceived = "RequestErrorNotificationReceived"
var disposeBag = DisposeBag()

typealias ServiceResponse = (JSON?, NSError?) -> Void

class Communication {
    
    var isMockData = false
    static let shared = Communication()
    let encodingQuery = URLEncoding(destination: .queryString)
    let encodingBody = URLEncoding(destination: .httpBody)
    let encodingJson = JSONEncoding.default
    
    func getHearders(_ auth : Bool = false) -> [String : String]{
        let u = User.getCurrentUser()
        var headers =
            [
              "Accept": "application/json",
              "Content-Type" : "application/json",
            ]
        if auth{
            Print("token \(u.Token)")
            headers["Authorization"] = "Bearer \(u.Token ?? "-1")"
        }
        return headers
    }
    func output(_ res : DataResponse<Data>) -> Void{
        Print("api tracking")
        var ans = ""
        if let urlString = res.request?.url?.absoluteString{
            ans += urlString + "\n"
            Print(urlString)
        }
        
        if let bod = res.request?.httpBody, let body = NSString(data: bod, encoding: String.Encoding.utf8.rawValue){
            ans += (body as String) + "\n"

            Print(body)
        }
        if let h = res.request?.allHTTPHeaderFields{
            ans += (h.toJSONString ?? "") + "\n"

            Print(h)
        }
        if let data = res.data, let s = NSString(data: data, encoding: String.Encoding.utf8.rawValue){
            ans += "\(s)"
            Print(s)
        }
    }
//    
//    func failure(object value: Any?){
//        notific.post(name: _RequestErrorNotificationReceived.not, object: RequestErrorNotificationRecived(object: value))
//    }
//    func ErrorRecive(object value: Any?){
//        notific.post(name: _RequestErrorNotificationReceived.not, object: RequestErrorNotificationRecived(object: value))
//    }
//    func Unauthenticated(object value: Any?){
//        (value as! CustomResponse).error_code = JSON(401);
//        notific.post(name: _RequestErrorNotificationReceived.not, object: RequestErrorNotificationRecived(object: value))
//    }
//    func ErrorData( object value: Any?){
//        (value as! CustomResponse).ErrorMessage = "Error in Data";
//        notific.post(name: _RequestErrorNotificationReceived.not, object: RequestErrorNotificationRecived(object: value))
//    }
    
    func RequestErrorNotificationRecived(object value: Any?) -> requestResult
    {
        var ans = requestResult()
        if let obj = value as? CustomResponse{
            var res : String = "";
            var errors = ""
            
            if let dic  = obj.data?.dictionaryObject{
                if let array = dic["errors"] as? [[String : Any]]{
                    for i in 0 ..< array.count{
                        errors = errors + array[i].description;
                        if i != array.count - 1{
                            errors += "\n"
                        }
                    }
                }
                else if let oneError = dic["error"] as? [String : Any] {
                    errors = errors + oneError.description;
                    errors += "\n"
                }
            }
            if  obj.Code == 401{
                Print("Auth")
                res = obj.message ;
                User.logout();
                ans = requestResult(isUnAuth: true)
            }
            else if obj.message != ""{
                res = obj.message;
                var title = ""
                var body = res
                if errors != "" {
                    title = res
                    body = errors
                }
                ans = requestResult(title: title, body: body)
            }else{
                res = obj.error;
                var title = ""
                var body = res
                if errors != "" {
                    title = res
                    body = errors
                }
                ans = requestResult(title: title, body: body)
            }
        }else{
            var res : String = "";
            res = value as? String ?? ""
            ans = requestResult(body: res)
        }
        return ans
    }
    
    /// call this function in order to call any end-point
    ///
    /// - warning: Be carefull! don't pass any value For VC in case to call it in background
    /// - parameter VC: the current ViewController , in case if you want to show,hide loader for the request
    /// - parameter Url: URL of the Request
    /// - parameter method: HTTPMethod of the Request
    /// - parameter params: Payload of the Request
    /// - parameter objectType: Model of the Request
    /// - returns: callback of objectType , callbackMessage of String value in case you want to show it
    func SendRequest<T: Decodable>(Url : String,method: HTTPMethod,params : [String : Any],objectType: T.Type)-> Observable<ApiResult<T, ApiErrorType>>{
        return Observable.create { [self] observer in
            let task = Alamofire.request(URL(string: Url)!, method: method,parameters: method != .get ? params : nil, encoding: self.encodingJson , headers: self.getHearders(true)).responseData() {
                (response ) in
                #if DEBUG
                self.output(response)
                #endif
                let statusCode = response.response?.statusCode ?? 0;
                switch response.result{
                    case .success(let value):
                        guard statusCode >= 200 , statusCode < 299 ,let m = try? JSONDecoder().decode(objectType, from:value) else {
                            observer.on(.next(.init(error: ApiErrorType.invalidJSON)))
                            observer.onCompleted()
                            return
                        }
                        guard statusCode != 401 else {
                            observer.on(.next(.init(error: ApiErrorType.unAuthenticated)))
                            observer.onCompleted()
                            return
                        }
                        guard statusCode != 403 else {
                            observer.on(.next(.init(error: ApiErrorType.unAuthorized)))
                            observer.onCompleted()
                            return
                        }
                        observer.on(.next(.init(value: m)))
                        observer.onCompleted()
                        break
                    case .failure(let error):
                        observer.on(.next(.init(error: ApiErrorType.unKnown(error.localizedDescription))))
                        observer.on(.error(error))
                        observer.onCompleted()
                        break
                }
            }
//            task.resume()
            return Disposables.create {
                task.cancel()
            }
        }
    }
    
    /// call this function in order to call any end-point
    ///
    /// - warning: Be carefull! the json file should be included in your project
    /// - parameter filename: name of json file
    /// - parameter objectType: Model of the Request
    /// - returns: model with type T?
    func loadJson<T: Decodable>(filename fileName: String,objectType: T.Type) -> T? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(objectType.self, from: data)
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
}

