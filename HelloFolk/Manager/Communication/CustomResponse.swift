//
//  CustomResponse.swift
//  HelloFolk
//
//  Created by Issam on 3/17/21.
//

import Foundation
import SwiftyJSON
import ObjectMapper
import AlamofireObjectMapper
import Alamofire

class CustomResponse : BaseEntity {
    
    var data : JSON!
    var message : String = ""
    var error : String = ""

    var Code : Int!
    var exception : String!

    var ErrorMessage : String = "";
    var error_code : JSON!
    var ErrorCollection : JSON!
    
    var description : String{
        return "CustomResponse: {data: \(data ?? "nil") , message : \(message), error_code : \(Code ?? -1)} ErrorMessage : \(ErrorMessage )"
    }
    
    override func mapping(map: Map) {

        data <- map["data"]
        message <- map["message"]
        error <- map["error"]
        ErrorMessage <- map["errors_massage"]
        ErrorCollection <- map["errors"]
        Code <- map["code"]
        exception <- map["exception"]
        
    }
    
}
struct requestResult {
    var title : String?
    var body : String?
    var isUnAuth : Bool?
}
enum ApiResult<Value, Error>{
    case success(Value)
    case failure(Error)

    init(value: Value){
        self = .success(value)
    }
    init(error: Error){
        self = .failure(error)
    }
}
//struct ApiErrorMessage: Codable{
//    var error_message: String?
//    var code: Int?
//}
enum ApiErrorType{
    case unKnown(String?)
    case unAuthenticated
    case unAuthorized
    case invalidJSON
    var description: String {
        get {
            switch self{
            case .unKnown(let val):
                return val ?? "Something Went Wrong"
            case .unAuthenticated:
              return "UnAuthenticated"
            case .unAuthorized:
              return "UnAuthorized"
            case .invalidJSON:
                return "Invalid JSON"
            }
        }
    }
}
struct ErrorResult {
    var title : String?
    var body : String?
}
