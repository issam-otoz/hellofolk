//
//  UIWindow.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import Foundation
import UIKit

extension UIWindow{
    func changeRootViewController(viewController:UIViewController){
        if self.rootViewController == nil {
            return
        }else{
            DispatchQueue.main.async {
                let snapShotView = self.snapshotView(afterScreenUpdates: true)
                viewController.view.addSubview(snapShotView!)
                self.rootViewController = viewController
                
                UIView.animate(withDuration: 0.3, animations: {
                    snapShotView?.layer.opacity = 0.0
                    snapShotView?.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5)
                }, completion: { (finished) in
                    snapShotView?.removeFromSuperview()
                })
            }
        }
    }
}
