//
//  String.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import Foundation
import UIKit
extension String{
    var localized:String{ return NSLocalizedString(self, comment: "") }
    
    var isEmptyStr:Bool{
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces).isEmpty
    }
    
    var trim:String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    var numbers:String{
        return self.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
    }
    
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }    
}
extension String {
    func height(constraintedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = self
        label.font = font
        label.sizeToFit()
        
        return label.frame.height
    }
    func width(constraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: .greatestFiniteMagnitude, height: height))
        label.numberOfLines = 0
        label.text = self
        label.font = font
        label.sizeToFit()
        
        return label.frame.width
    }
}

extension String{
    func htmlPretty(fontFamily : String,fontWeight :String,fontSize :String,fontColor :String) -> String{
        let startHtmlString = """
        <style>
        @font-face
        {
            font-family: 'Poppins';
            font-weight: normal;
            src: url(Poppins-Regular.ttf);
        }
        @font-face
        {
            font-family: 'Poppins';
            font-weight: bold;
            src: url(Poppins-Bold.ttf);
        }
        @font-face
        {
            font-family: 'Poppins';
            font-weight: 300;
            src: url(Poppins-Medium.ttf);
        }
        @font-face
        {
            font-family: 'Poppins';
            font-weight: 200;
            src: url(Poppins-Light.ttf);
        }
        @font-face
        {
            font-family: 'Poppins';
            font-weight: 500;
            src: url(Poppins-SemiBold.ttf);
        }
        </style>
        <div style="font-family: '\(fontFamily)'; font-weight: \(fontWeight); p{margin-bottom: 0px;} font-size: \(fontColor); color: \(fontColor)">
        """
        let endHtmlString = """
        </div>
        """
        let meta = "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1,maximum-scale=1.0, minimum-scale=1.0, user-scalable=no\">"
        let html = meta + startHtmlString + (self) + endHtmlString
        
        return html
    }
}
//        <div style="font-family: '\(fontFamily)'; font-weight: \(fontWeight); font-size: \(fontColor); line-height:16px; color: \(fontColor)">
extension String {
    /**
     true iff self contains characters.
     */
    public var isNotEmpty: Bool {
        return !isEmpty
    }
}
