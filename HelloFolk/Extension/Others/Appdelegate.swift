//
//  Appdelegate.swift
//  HelloFolk
//
//  Created by Issam on 3/18/21.
//

import Foundation
import UIKit
extension AppDelegate{
    func printAllFonts(){
        #if DEBUG
        UIFont.familyNames.forEach({ familyName in
                   let fontNames = UIFont.fontNames(forFamilyName: familyName)
                   Print(familyName, fontNames)
        })
        #endif
    }
}
