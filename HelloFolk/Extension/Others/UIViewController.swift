//
//  UIViewController.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import Foundation
import UIKit

extension UIViewController {
    class var storyboardID:String{
        return "\(self)"
    }
       
    static func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController?{
        if let navigationController = controller as? UINavigationController{
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController{
            if let selected = tabController.selectedViewController{
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController{
            return topViewController(controller: presented)
        }
        return controller
    }
    
}

extension UIViewController
{
    // Screen width.
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    // Screen height.
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    func displayMyAlertMassge(usermassge : String,title : String = ""){
        
        let myalert = UIAlertController(title: title, message: usermassge, preferredStyle: UIAlertController.Style.alert);
        let okaction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil);
        myalert.addAction(okaction)
        self.present(myalert,animated:true,completion:nil);
    }
    
    func i_changeVC(name : String,storyboard : String = "Main"){
        let storyboard = UIStoryboard.init(name: storyboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: name)
        vc.modalPresentationStyle = .fullScreen;
        self.present(vc, animated: true, completion: nil);
    }
    func i_PushView(name : String,storyboard : String = "Main",animated : Bool = false){
        let storyboard = UIStoryboard.init(name: storyboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: name)
        self.navigationController?.pushViewController(vc, animated: animated)
    }
    func i_popViewControllerss(popViews: Int, animated: Bool = true) {
        if self.navigationController!.viewControllers.count > popViews
        {
            let vc = self.navigationController!.viewControllers[self.navigationController!.viewControllers.count - popViews - 1]
             self.navigationController?.popToViewController(vc, animated: animated)
        }
    }
    func i_HtmlAttributedString(HtmlString : String) -> NSAttributedString{
        var htmlString = "<html>" +
            "<head>" +
            "<style>" +
            "body {" +
            "background-color: rgb(230, 230, 230);" +
            "font-family: 'Arial';" +
            "text-decoration:none;" +
            "}" +
            "</style>" +
            "</head>" +
            "<body>" +
            "<h1>A title</h1>" +
            "<p>A paragraph</p>" +
            "<b>bold text</b>" +
        "</body></html>"
        htmlString = HtmlString;
        
        let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)
        
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
        
        let attributedString : NSAttributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
        
        return attributedString;
    }
//    func i_rootView(name : String){
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let vc = storyboard.instantiateViewController(withIdentifier: name)
//        appDelegate.window?.rootViewController = vc
//    }
    
    
    
}
extension AppDelegate{
    static func i_rootView(name : String , storyboard : String = "Main"){
        let storyboard = UIStoryboard.init(name: storyboard, bundle: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let vc = storyboard.instantiateViewController(withIdentifier: name)
        appDelegate.window?.rootViewController = vc
    }
}

extension UIViewController{
    
    public var shouldShowMessages: Bool
    {
        get {
                return self.viewIfLoaded?.window != nil
            }
    }
}
extension UIViewController {

    func setupNavigationMultilineTitleNew(_ MyTitle : String){
        let label = UILabel()
            label.backgroundColor = .clear
            label.numberOfLines = 5
            label.font = ThemeApp.Fonts.medium16
            label.textAlignment = .center
            label.textColor = .white
            label.text = MyTitle
            self.navigationItem.titleView = label
    }
    func setupNavigationMultilineTitle(_ MyTitle : String) {
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = false
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationItem.largeTitleDisplayMode = .never
        } else {
            // Fallback on earlier versions
        }

    self.title = MyTitle
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.largeTitleTextAttributes = [
                NSAttributedString.Key.foregroundColor: UIColor.black,
                NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .title1)
            ]
        } else {
            // Fallback on earlier versions
        }

    for navItem in(self.navigationController?.navigationBar.subviews)! {
         for itemSubView in navItem.subviews {
             if let largeLabel = itemSubView as? UILabel {
                 largeLabel.text = self.title
                 largeLabel.numberOfLines = 0
                 largeLabel.lineBreakMode = .byWordWrapping
                 largeLabel.sizeToFit()
                self.navigationController?.navigationBar.frame.size.height =  50 + largeLabel.frame.height
             }
         }
    }
}
    func setMultilineNavigationBar(topText:  String, bottomText : String) {
         let topTxt = NSLocalizedString(topText, comment: "")
         let bottomTxt = NSLocalizedString(bottomText, comment: "")

         let titleParameters = [NSAttributedString.Key.foregroundColor : UIColor.white,
                                   NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .semibold)]
         let subtitleParameters = [NSAttributedString.Key.foregroundColor : UIColor.white,
                                      NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13, weight: .regular)]

         let title:NSMutableAttributedString = NSMutableAttributedString(string: topTxt, attributes: titleParameters)
         let subtitle:NSAttributedString = NSAttributedString(string: bottomTxt, attributes: subtitleParameters)

         title.append(NSAttributedString(string: "\n"))
         title.append(subtitle)

         let size = title.size()

         let width = size.width
         guard let height = navigationController?.navigationBar.frame.size.height else {return}

          let titleLabel = UILabel(frame: CGRect.init(x: 0, y: 0, width: width, height: height))
          titleLabel.attributedText = title
          titleLabel.numberOfLines = 0
          titleLabel.textAlignment = .center
          self.navigationItem.titleView = titleLabel
        }
}
