//
//  Optional.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import Foundation

extension Optional where Wrapped == String{
    var stringValue:String{
        switch self {
        case .some(let value):
            return value
        default:
            return ""
        }
    }
    
    func isEmpty() -> Bool {
        if self == nil{
            return true
        }
        
        switch self {
        case .some(let value):
            if value.trim.count <= 0 {
                return true
            }else{
                return false
            }
        default:
            return false
        }
    }
    
//    func date() -> Date?{
//        if self == nil {
//            return nil
//        }
//        
//        switch self {
//        case .some(let value):
//            return value.toDate("yyyy-MM-dd HH:mm:ss.SSSSSS", region: SwiftDate.defaultRegion)?.date
//        default:
//            return nil
//        }
//    }
//    
//    func convertDateToRegion(region:Region) -> Date? {
//        switch self {
//        case .some(let value):
//            return value.toDate("yyyy-MM-dd HH:mm:ss.SSSSSS", region: region)?.date
//        default:
//            return nil
//        }
//    }
//    
//    func convertDateToRegionWithoutMS(region:Region) -> Date? {
//        switch self {
//        case .some(let value):
//            return value.toDate("yyyy-MM-dd HH:mm:ss", region: region)?.date
//        default:
//            return nil
//        }
//    }
}
