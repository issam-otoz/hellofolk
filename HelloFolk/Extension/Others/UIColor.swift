//
//  UIColor.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import UIKit

extension UIColor {
    convenience init(hexString:String,alpha:CGFloat=1.0) {
        let hexString:NSString = hexString.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines) as NSString
        let scanner            = Scanner(string: hexString as String)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color:UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    
    static var primary:UIColor {
        return UIColor(hexString: "024B30")
    }
    
    static var errorColor:UIColor {
        return UIColor(hexString: "ff1844")
    }
}

extension UIColor {
    class var borderColor:UIColor {
       return UIColor(hexString: "E0E0E0")
   }
}
extension UIColor{
    
    public convenience init(_ hex : String) {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            self.init(red: 255/255, green: 0, blue: 0, alpha: 1)// UIColor.gray
            return
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}



extension UIColor {
    static func rgba(red:CGFloat , green:CGFloat , blue:CGFloat , alpha:CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
    
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
    
    struct UIDesign {
        static let white           = UIColor.rgb(red: 255, green: 255, blue: 255)
        static let dark            = UIColor.rgb(red: 63, green: 61, blue: 86)
        static let entryBorder     = UIColor.rgb(red: 220, green: 220, blue: 220)
        static let primaryBlue     = UIColor.rgb(red: 80, green: 100, blue: 134)
        static let separetor       = UIColor.rgb(red: 112, green: 112, blue: 112)
        static let orange          = UIColor.rgb(red: 255, green: 111, blue: 94)
        static let green           = UIColor.green
        static let darkPlaceholder = UIColor.rgba(red: 63, green: 61, blue: 86, alpha: 0.5)
        static let grayLabel       = UIColor.rgb(red: 160, green: 160, blue: 160)
        static let textValue       = UIColor.rgb(red: 108, green: 107, blue: 124)
        static let greyedOutEntry  = UIColor.rgb(red: 245, green: 245, blue: 245)
        static let sideImgColored  = UIColor.rgb(red: 63, green: 40, blue: 146)
    }
}
