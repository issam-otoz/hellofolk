//
//  UILabel.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import UIKit

extension NSMutableAttributedString{
// If no text is send, then the style will be applied to full text
    func setColorForText(_ textToFind: String?, with color: UIColor) {
        
        let range:NSRange?
        if let text = textToFind{
            range = self.mutableString.range(of: text, options: .caseInsensitive)
        }else{
            range = NSMakeRange(0, self.length)
        }
        if range!.location != NSNotFound {
            addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range!)
        }
    }
}

extension UILabel {
    
    func setupRequiredStar() {
        let str = self.text
        let trimmedString = str!.trimmingCharacters(in: .whitespacesAndNewlines)
        let string = NSMutableAttributedString(string: trimmedString)
        string.setColorForText("*", with: ThemeApp.Colors.primary)
        self.attributedText = string
    }
    
}
