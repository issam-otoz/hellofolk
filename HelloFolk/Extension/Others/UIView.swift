//
//  UIView.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import Foundation
import UIKit

extension UIView {
    func addShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowColor = UIColor.black.withAlphaComponent(0.4).cgColor
        //self.layer.shadowRadius = 7
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 0.6
        self.layer.shouldRasterize = true
        self.layer.cornerRadius = 8
        self.layer.rasterizationScale = UIScreen.main.scale
        self.layer.backgroundColor =  UIColor.white.cgColor
    }
}
public extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var shadowOffsetHeight: CGFloat {
        get {
            return layer.shadowOffset.height
        }
        set {
            layer.shadowOffset.height = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    
    @IBInspectable var shadowOffsetWidth: CGFloat {
        get {
            return layer.shadowOffset.width
        }
        set {
            layer.shadowOffset.width = newValue
        }
    }
    
    
    @IBInspectable var shadowColor: CGColor? {
        get {
            return layer.shadowColor
        }
        set {
            layer.shadowColor = newValue
        }
    }
    
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    func setCardView()
    {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 5)
        layer.shadowOpacity = 0.5
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath

    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor.init(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }


    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
    }
}
extension UIScrollView {
    func takeAScreenShot(VC : UIViewController){
        guard let snapshot = self.subviews[0].snapshotViewHierarchy() else {
            return
        }
        VC.shareImageOfVC(img: snapshot )
    }
    func screenshot() -> UIImage? {
        let scrollView = self
        UIGraphicsBeginImageContext(scrollView.contentSize)

        let savedContentOffset = scrollView.contentOffset
                let savedFrame = scrollView.frame

                scrollView.contentOffset = CGPoint.zero
                scrollView.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)

                scrollView.layer.render(in: UIGraphicsGetCurrentContext()!)
                let image = UIGraphicsGetImageFromCurrentImageContext()

                scrollView.contentOffset = savedContentOffset
                scrollView.frame = savedFrame

                UIGraphicsEndImageContext()

                return image
    }
}

extension UIViewController{
    func shareImageOfVC(img: UIImage) {

            // image to share
            var image = UIImage(named: "Image")
            image = img

            // set up activity view controller
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

            // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]

            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
        }
}
public extension UIView {
    /**
        Captures view and subviews in an image
    */
    func snapshotViewHierarchy() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        let copied = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return copied
    }
}
extension UIView{
    func subViewsType<T: UIView>(of type: T.Type) -> [T] {
    var results = [T]()
        for subview in self.subviews as [UIView] {
        if let t = subview as? T {
            results += [t]
        }else {
            results += subview.subViewsType(of: T.self)
        }
    }
    return results
    }
    func getCollections(view: UIView) -> [UICollectionView] {
    var results = [UICollectionView]()
    for subview in view.subviews as [UIView] {

        if let textField = subview as? UICollectionView {
            results += [textField]
        } else if let textField = subview as? UITableView{
            continue
            
        }else {
            results += getCollections(view: subview)
        }
    }
    return results
    }
    
}
