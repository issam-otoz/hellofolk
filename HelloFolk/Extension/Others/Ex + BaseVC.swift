//
//  Ex + BaseVC.swift
//  HelloFolk
//
//  Created by Issam on 8/11/21.
//

import Foundation
import UIKit
import SideMenu
import SDWebImage
import WXImageCompress
import SwiftyJSON
import Lottie
import RxSwift
import RxCocoa
import Then

enum ProgressKeyFrames: CGFloat {
  case start = 0
  case end = 41
}

extension BaseVC{
    func registerReceivers()
    {
        notific.addObserver(self, selector: #selector(self.RequestErrorNotificationRecived(_:)), name: _RequestErrorNotificationReceived.not, object: nil)
    }
    
    func unregisterReceivers()
    {
        notific.removeObserver(self, name: _RequestErrorNotificationReceived.not, object: nil)
    }
    
    /// call any Fetch , Send Request under this Function so The VC could displayTryAgain if any error happen in Fetch Data from VMs
    ///
    /// - returns: void
    @objc func getRefresh(){
        if isfetchData{
            fetchData()
        }else{
            sendData()
        }
    }
    @objc func fetchData(){
        isfetchData = true
        self.ref.endRefreshing()

    }
    @objc func sendData(){
        isfetchData = false

    }
    
    /// override this function in your child VC in order to setup Views of Your VC
    ///
    /// - returns: void
    @objc func setupViews(){
    }
    
    /// override this function in your child VC in order to setup Style of Your VC
    ///
    /// - returns: void
    @objc func setupStyle(){
        
    }
    /// call this function in order to display TryAgain,Cancel in case any error happen
    ///
    /// - parameter title: title of your the UIAlertController
    /// - parameter usermassge: body of your the UIAlertController
    /// - returns: void
    func displayTryAgain(title : String = "",usermassge : String){
        
        let myalert = UIAlertController(title: title, message: usermassge, preferredStyle: UIAlertController.Style.alert);
        let okaction = UIAlertAction(title:Constants.TryAgain, style: UIAlertAction.Style.default,handler: { action in
            self.getRefresh()
        })
        let Cancelaction = UIAlertAction(title: Constants.Cancel, style: UIAlertAction.Style.default, handler: { action in
            self.OnCancelledResponse();
        })
        myalert.addAction(okaction)
        myalert.addAction(Cancelaction)
        self.present(myalert,animated:true,completion:nil);
    }
    @objc func RequestErrorNotificationRecived(_ notification : NSNotification)
    {
        self.hideLoading()
        if let obj = notification.object as? ErrorResult{
            displayTryAgain(title:obj.title ?? "" ,usermassge: obj.body ?? "");
        }
    }
    
    /// call this function in order to display Loading
    ///
    /// - returns: void
    func showLoading(){
        if Thread.isMainThread {
            showLoadingFunc()
        }else{
            DispatchQueue.main.async {
                self.showLoadingFunc()
            }
        }
    }
    
    func showLoadingFunc(){
        progressView.contentMode = .scaleAspectFit
        if hudSuperV != nil{
            progressView.frame = progressView.bounds
            hudSuperV.addSubview(progressView)
        }else{
            progressView.frame = view.bounds
            view.addSubview(progressView)
        }
        progressView.play(fromFrame: ProgressKeyFrames.start.rawValue, toFrame: ProgressKeyFrames.end.rawValue, loopMode: .loop)
    }
    
    /// call this function in order to hide Loading
    ///
    /// - returns: void
    func hideLoading(){
        if Thread.isMainThread {
            ref.endRefreshing()
            progressView.stop()
            progressView.removeFromSuperview()
        }else{
            DispatchQueue.main.async {
                self.ref.endRefreshing()
                self.progressView.stop()
                self.progressView.removeFromSuperview()
            }
        }
        
    }
    
    /// override this function in your child VC in order to handle Cancel action for displayTryAgain UIAlertController
    ///
    /// - returns: void
    @objc func OnCancelledResponse(){
        self.navigationController?.popViewController(animated: true)
    }
    
    /// call this function to open Navgation Menu
    ///
    /// - returns: void
    @objc func openNavgationMenu(){
        present(SideMenuManager.default.leftMenuNavigationController!, animated: true, completion: nil)
    }

    @objc func onBackBtnTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    /// call this function to add menu button
    ///
    /// - returns: void
    @objc func setupLeftBtn(){
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        //
        let btnShowMenu = UIButton(type: UIButton.ButtonType.system).then{
            $0.setImage(UIImage(named: "menu"), for: UIControl.State())
            $0.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            $0.tintColor = .black
            $0.addTarget(self, action: #selector(openNavgationMenu), for: UIControl.Event.touchUpInside)
        }
        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
        self.navigationItem.leftBarButtonItem = customBarItem;
    }
    
    /// call this function setup Right Button in navigation bar
    ///
    /// - parameter selector: Selector action Right Button
    /// - parameter img: image of Button
    /// - parameter btnTitle: title of Button
    /// - parameter tint: Color of Button
    /// - returns: void
    @objc func setupRightBtn(selector: Selector, img: String? = nil,btnTitle : String? = nil, tint: UIColor = .black){
        let editContact = UIButton(type: UIButton.ButtonType.system).then{
            if img != nil{
                $0.setImage(UIImage(named: img ?? ""), for: UIControl.State())
            }
            if btnTitle != nil {
                $0.setTitle(btnTitle ?? "", for: .normal)
            }
            $0.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            $0.tintColor = tint
            $0.layer.cornerRadius = 5
            $0.addTarget(self, action: selector, for: .touchUpInside)
        }
        let customBarItem = UIBarButtonItem(customView: editContact)
        self.navigationItem.rightBarButtonItem = customBarItem
    }
    
    /// call this function to get  default Menu Image
    ///
    /// - returns: UIImage
    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)
        
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()
        
        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return defaultMenuImage;
    }
    
    /// call this function setup SideMenu
    ///
    /// - returns: void
    func setupSideMenu(){
        
        let storyboard = UIStoryboard.init(name: Constants.StoryBoard.Onboarding.rawValue, bundle: nil)

        let menuController = storyboard.instantiateViewController(withIdentifier: Constants.SideMenuVC) as! SideMenuVC
        
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: menuController)
        
        var settings = SideMenuSettings()
        settings.menuWidth = UIScreen.main.bounds.width * 3 / 4
        settings.presentationStyle = .menuSlideIn
        settings.dismissWhenBackgrounded = true
        menuLeftNavigationController.settings = settings
        menuLeftNavigationController.leftSide = true
        menuLeftNavigationController.isNavigationBarHidden = true
        SideMenuManager.default.leftMenuNavigationController = menuLeftNavigationController
    }
}

extension BaseHelloFolkVC{
    
    func setupStatusBar(barStyle: UIBarStyle = UIBarStyle.blackOpaque){
        UINavigationBar.appearance().barStyle =  barStyle
    }
    
    func setupBackBtn(){
        if navBarView == nil { return }
        let btn = UIButton().then{
            leftbtnBarView = $0

            $0.translatesAutoresizingMaskIntoConstraints = false
            navBarView!.addSubview($0)
            $0.centerYAnchor.constraint(equalTo: navBarView!.centerYAnchor, constant: 0).isActive = true
            $0.leadingAnchor.constraint(equalTo: navBarView!.leadingAnchor, constant: 8).isActive = true
            $0.heightAnchor.constraint(equalToConstant: 50).isActive = true
            $0.widthAnchor.constraint(equalToConstant: 50).isActive = true
        }
        
        let _ = UIImageView().then{
            $0.image = UIImage(named: Constants.BackImg)
            $0.contentMode = .scaleAspectFit
            $0.translatesAutoresizingMaskIntoConstraints = false
            btn.addSubview($0)
            $0.centerYAnchor.constraint(equalTo: btn.centerYAnchor, constant: 0).isActive = true
            $0.centerXAnchor.constraint(equalTo: btn.centerXAnchor, constant: 0).isActive = true
            $0.heightAnchor.constraint(equalToConstant: 19).isActive = true
            $0.widthAnchor.constraint(equalToConstant: 25).isActive = true
            btn.addTarget(self, action: #selector(onBackBtnTapped), for: UIControl.Event.touchUpInside)
        }
    }
    
    func setupNavBar(){

        if Provider.shared.baseNavBarVC != nil &&  Provider.shared.baseNavBarVC?.navBarView != nil{
            navBarView = Provider.shared.baseNavBarVC?.navBarView
            return
        }
        guard self.navigationController?.navigationBar != nil else{
            return
        }
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
        }

        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        // Add shadow and cirner radius to navbar
        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        let shadowView = UIView(frame: CGRect(x: 0, y: -statusBarHeight,width: (self.navigationController?.navigationBar.bounds.width)! ,height: (self.navigationController?.navigationBar.bounds.height)! + statusBarHeight )).then{
            navBarView = $0
            $0.backgroundColor = UIColor.white
            self.navigationController?.navigationBar.insertSubview($0, at: 1)
        }
        
        let _ = CAShapeLayer().then {
            $0.path = UIBezierPath(roundedRect: shadowView.bounds, byRoundingCorners: [.bottomLeft , .bottomRight ], cornerRadii: CGSize(width: 20, height: 20)).cgPath
            $0.fillColor = ThemeApp.Colors.navBar.cgColor
            $0.shadowColor = UIColor.white.cgColor
            $0.shadowPath = $0.path
            $0.shadowOffset = CGSize(width: 2.0, height: 2.0)
            $0.shadowOpacity = 0.8
            $0.shadowRadius = 2
            shadowView.layer.insertSublayer($0, at: 0)
        }
        
        let _ = UIImageView().then {
            shadowView.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.image = UIImage(named: Constants.LogoImg)
            $0.centerYAnchor.constraint(equalTo: shadowView.centerYAnchor, constant: 0).isActive = true
            $0.centerXAnchor.constraint(equalTo: shadowView.centerXAnchor, constant: 0).isActive = true
            $0.heightAnchor.constraint(equalToConstant: 24).isActive = true
            $0.widthAnchor.constraint(equalToConstant: 117).isActive = true
        }
        
    }
}
