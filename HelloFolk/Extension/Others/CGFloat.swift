//
//  CGFloat.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import UIKit

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

 extension Double{
    func convertDoubleToCurrency() -> String{
        let formatter = NumberFormatter()
        formatter.locale = Locale.current // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
        formatter.numberStyle = .currency
        formatter.currencyCode = "AED"
        formatter.currencySymbol = ""
        if let formattedTipAmount = formatter.string(from: self as NSNumber) {
//            tipAmountLabel.text = "Tip Amount: \(formattedTipAmount)"
            return formattedTipAmount
        }else{
            return "Error number Format"
        }
    }
    
}
