//
//  KeyboardMessageViewSegue.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import SwiftMessages

open class KeyboardMessageViewSegue: SwiftMessagesSegue {
    override init(identifier: String?, source: UIViewController, destination: UIViewController) {
        super.init(identifier: identifier, source: source, destination: destination)
        configure(layout: .bottomCard)
        //        self.presentationStyle = .top
        self.dimMode =  .color(color: UIColor.init(displayP3Red: 0, green: 0, blue: 0, alpha: 0.6), interactive: true)
        // (style: .dark, alpha: 0.8, interactive: true)

        self.interactiveHide = false
        messageView.configureNoDropShadow()
        
        let keyboardHeight = KeyboardService.keyboardHeight() + 20
//        let keyboardHeight = CGFloat(0) + 20

        messageView.layoutMarginAdditions = UIEdgeInsets(top: 40, left: -10, bottom: keyboardHeight, right: -10)
        
    }
}
