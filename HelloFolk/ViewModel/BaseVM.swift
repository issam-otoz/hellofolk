//
//  BaseVM.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import Then
class BaseVM : NSObject{
    var disposeBag = DisposeBag()

    var didUpdateData: Box<Bool?> = Box(false)
    
    var errorResult : Driver<ErrorResult>

    override init() {
        self.errorResult = Driver.never()
        errorResult.drive(onNext: { res in
            notific.post(name: _RequestErrorNotificationReceived.not, object: res)
        },onCompleted: {
        },onDisposed: {
        }).disposed(by: disposeBag)
        super.init()
    }
    
    func RequestErrorHandler(_ error : ApiErrorType)
    {
        switch error {
        case .unAuthenticated:
            notific.post(name: _RequestErrorNotificationReceived.not, object: ErrorResult.init(title: "ops", body: error.description))
            //handle unAuthenticated
            break
        case .unAuthorized:
            notific.post(name: _RequestErrorNotificationReceived.not, object: ErrorResult.init(title: "ops", body: error.description))
            //handle unAuthenticated
            break
        case .invalidJSON:
            notific.post(name: _RequestErrorNotificationReceived.not, object: ErrorResult.init(title: "ops", body: error.description))
            //handle unAuthenticated
            break
        case .unKnown(_):
            notific.post(name: _RequestErrorNotificationReceived.not, object: ErrorResult.init(title: "ops", body: error.description))
            //handle unAuthenticated
            break
        }
    }
}
