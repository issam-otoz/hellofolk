//
//  Box.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import Foundation

class Box<T> {
    typealias Listener = (T) -> Void
    var listner : Listener?
    
    var value : T {
        didSet{
            listner?(value)
        }
    }
    init(_ value : T) {
        self.value = value
    }
    
    func bind(listner : Listener?){
        self.listner = listner
        listner?(value)
    }
}
