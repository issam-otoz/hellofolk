//
//  NewsVM.swift
//  HelloFolk
//
//  Created by Issam on 3/19/21.
//

import Foundation
import UIKit
import Then
import RxSwift
import RxCocoa

class NewsVM : BaseVM{
    
    var arrayData : postsM!{
        didSet{
            self.didUpdateData.value = !(arrayData.isEmpty)
        }
    }
    var posts : postsM = []
    var users : usersM = []

    override init() {
        arrayData = []
        super.init()
    }
    func updateData(_ posts : postsM ,_ users : usersM){
        var temp : [Int:userM] = [:]
        for i in users{
            temp[i.id ?? -1] = i
        }
        var tempPosts : postsM = posts
        for i in 0 ..< tempPosts.count{
            tempPosts[i].user = temp[tempPosts[i].id ?? -1]
        }
        arrayData = tempPosts
    }
    func getPosts() -> Observable<postsM>{
        return Observable.create { [self] observer in
            if !posts.isEmpty{ //cach from VM
                observer.on(.next(posts))
                observer.onCompleted()
            }else if !(User.getCurrentUser().posts ?? []).isEmpty{ //cach from memory
                posts = User.getCurrentUser().posts ?? []
                observer.on(.next(posts))
                observer.onCompleted()
            }else{ //get data from api for the first time
                let rx = Communication.shared.getPosts()
                rx
                  .subscribe(onNext: { res in
                    switch res{
                    case .success(let value):
                        let u = User.getCurrentUser()
                        u.posts = value
                        User.saveMe(me: u)
                        RS.shared.newsVM.posts = value
                        observer.on(.next(value))
                        break
                    case .failure(let error):
                        RequestErrorHandler(error)
                        break
                    }
                    observer.onCompleted()
                  },onError: { (error) in
                    RequestErrorHandler(.unKnown(error.localizedDescription))
                    observer.onCompleted()
                  },onCompleted: {
                    observer.onCompleted()
                  })
                  .disposed(by: disposeBag)
            }
            return Disposables.create()
        }
    }
    func getUsers() -> Observable<usersM>{
        return Observable.create { [self] observer in
            if !users.isEmpty{ //cach from VM
                observer.on(.next(users))
                observer.onCompleted()
            }else if !(User.getCurrentUser().usersPosts ?? []).isEmpty{ //cach from memory
                users = User.getCurrentUser().usersPosts ?? []
                observer.on(.next(users))
                observer.onCompleted()
            }else{ //get data from api for the first time
                let rx = Communication.shared.getUsers()
                rx
                  .subscribe(onNext: { res in
                    switch res{
                    case .success(let value):
                        let u = User.getCurrentUser()
                        u.usersPosts = value
                        User.saveMe(me: u)
                        users = value
                        observer.on(.next(value))
                        break
                    case .failure(let error):
                        RequestErrorHandler(error)
                        break
                    }
                    observer.onCompleted()
                  },onError: { (error) in
                    RequestErrorHandler(.unKnown(error.localizedDescription))
                    observer.onCompleted()
                  },onCompleted: {
                    observer.onCompleted()
                  })
                  .disposed(by: disposeBag)
            }
            return Disposables.create()
        }
        
        
    }
    
    func fetchData() -> Observable<postsM>{
        return Observable.create { [self] observer in
            let rx: Observable<(postsM, usersM)> = Observable.combineLatest(getPosts(), getUsers())
            rx.subscribe(onNext: { res in
                var temp : [Int:userM] = [:]
                for i in res.1{
                    temp[i.id ?? -1] = i
                }
                var tempPosts : postsM = res.0
                for i in 0 ..< tempPosts.count{
                    tempPosts[i].user = temp[tempPosts[i].id ?? -1]
                }
                arrayData = tempPosts
                observer.on(.next(tempPosts))
                observer.onCompleted()
              },onError: { (error) in
                RequestErrorHandler(.unKnown(error.localizedDescription))
                observer.onCompleted()
              },onCompleted: {
                observer.onCompleted()
              })
              .disposed(by: disposeBag)
        
        return Disposables.create()
        }
    }

}
