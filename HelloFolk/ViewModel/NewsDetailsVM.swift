//
//  NewsDetailsVM.swift
//  HelloFolk
//
//  Created by Issam on 3/19/21.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import Then

extension PostM: Then {}

class NewsDetailsVM : BaseVM{
    var data : PostM
    override init() {
        data = PostM()
        super.init()
    }
    func updateData(_ post : PostM ,_ comments : CommentsM? = nil){
        let temp = post.with {
            $0.comments = comments
        }
        data = temp
    }
    func fetchData()  -> Observable<PostM> {
        return Observable.create { [self] observer in
            let rx = Communication.shared.getCommentsPost(postId: self.data.id ?? -1)
            rx
              .subscribe(onNext: { res in
                switch res{
                case .success(let value):
                    let temp = self.data.with {
                        $0.comments = value
                    }
                    data = temp
                    observer.on(.next(data))
                    break
                case .failure(let error):
                    RequestErrorHandler(error)
                    break
                }
                observer.onCompleted()
              },onError: { (error) in
                RequestErrorHandler(.unKnown(error.localizedDescription))
                observer.onCompleted()
              },onCompleted: {
                observer.onCompleted()
              })
              .disposed(by: disposeBag)
            
            return Disposables.create()
        }

    }
}

