//
//  SplashVC.swift
//  HelloFolk
//
//  Created by Issam on 3/17/21.
//

import Foundation
import UIKit
class SplashVC : BaseHelloFolkVC {
    var arrayImgs = [Constants.Splash0 , Constants.Splash1 , Constants.Splash2 , Constants.Splash3]
    @IBOutlet weak var img: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.accessibilityIdentifier = Constants.SplashVC
    }
    override func setupViews() {
        super.setupViews()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animationSplash(arrayImgs: arrayImgs, ind: 0 , duration : 1) { (res) in
            self.i_changeVC(name: Constants.CoverVC)
        }
    }
    func animationSplash(arrayImgs: [String] , ind : Int,duration : TimeInterval,callback : @escaping (Bool) -> Void){
        UIView.transition(with: self.view, duration: duration, options: [.transitionCrossDissolve], animations: {
            self.img.image = UIImage(named: arrayImgs[ind])
            if ind+1 < arrayImgs.count
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + duration ) {
                    self.animationSplash(arrayImgs: arrayImgs, ind: ind+1, duration: duration) { (res) in
                        callback(res)
                    }
                }
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + duration ) {
                    callback(true)
                }
            }
        }, completion: nil)
    }
}
