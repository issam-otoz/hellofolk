//
//  Splash1VC.swift
//  HelloFolk
//
//  Created by Issam on 3/17/21.
//

import UIKit
import Lottie
class OnBoardingSplashVC: BaseHelloFolkVC {
    @IBOutlet weak var lottieView: AnimationView!
    @IBOutlet weak var titlelbl: HelloFolkBLabel!
    @IBOutlet weak var bodylbl: HelloFolkBLabel!
    var isSecondSpalsh = false
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func setupViews(){
        
        titlelbl.textValue = Constants.TitleLorem
        bodylbl.textValue = Constants.BodyLorem
        
        if isSecondSpalsh{
            view.accessibilityIdentifier = Constants.OnBoardingSplash2VC
            lottieSetup(name: Constants.RdhvLottie)
        }else{
            view.accessibilityIdentifier = Constants.OnBoardingSplash1VC
            lottieSetup()
        }
        
    }
    func lottieSetup(name : String = Constants.NhvmLottie){
        
        let animation = Animation.named(name)
        lottieView.animation = animation
        lottieView.contentMode = .scaleAspectFit
        lottieView.loopMode = .loop
        lottieView!.animationSpeed = 1
        lottieView!.play()
    }
    
}
