//
//  CoverVC.swift
//  HelloFolk
//
//  Created by Issam on 3/18/21.
//

import UIKit

class CoverVC: BaseHelloFolkVC {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var nextbtn: UIButton!
    @IBOutlet weak var backbtn: UIButton!
    var ArrayRouteView : [BaseVC] = [];
    var isSecondSpalsh = false

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func setupViews() {
        super.setupViews()
        backbtn.isHidden = true
        let storyboard = UIStoryboard.init(name: Constants.StoryBoard.Main.rawValue, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Constants.OnBoardingSplashVC)
        (vc as? OnBoardingSplashVC)?.isSecondSpalsh = false
        self.addCrumb(vc as! BaseHelloFolkVC)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
         .default
    }
    
}

extension CoverVC{
    @IBAction func next(_ sender: Any) {
        if isSecondSpalsh {
            setupStatusBar()
            i_changeVC(name: Constants.NavHomeVC)
        }else{
            isSecondSpalsh = true
            let storyboard = UIStoryboard.init(name: Constants.StoryBoard.Main.rawValue, bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: Constants.OnBoardingSplashVC)
            (vc as? OnBoardingSplashVC)?.isSecondSpalsh = true
            self.addCrumb(vc as! BaseHelloFolkVC)

        }
        backbtn.isHidden = false
    }
    @IBAction func back(_ sender: Any) {
        isSecondSpalsh = false
        backbtn.isHidden = true
        popVC()
    }
}


// extention BreadCrumb
extension CoverVC{
    func popVC(){
        if ArrayRouteView.count > 1 {
            UIView.transition(with: self.containerView, duration: 0.5, options: [.transitionCurlUp], animations: {
            }, completion: nil)
            let i = ArrayRouteView.count - 1
            ArrayRouteView[i].willMove(toParent: nil)
            ArrayRouteView[i].view.removeFromSuperview()
            ArrayRouteView[i].removeFromParent()
            ArrayRouteView.removeLast()
        }
  
    }
   func addCrumb(_ controller : BaseHelloFolkVC){
       ArrayRouteView.append(controller)
       addChild(controller)
       UIView.transition(with: self.containerView, duration: 0.5, options: [.transitionCurlDown], animations: {
           self.containerView.addSubview(controller.view)
       }, completion: nil)
       controller.view.translatesAutoresizingMaskIntoConstraints = false
       controller.view.topAnchor.constraint(equalTo: self.containerView.topAnchor, constant: 0).isActive = true
       controller.view.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor, constant: 0).isActive = true
       controller.view.rightAnchor.constraint(equalTo: self.containerView.rightAnchor, constant: 0).isActive = true
       controller.view.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant: 0).isActive = true
       controller.didMove(toParent: self)
   }
    
}
