//
//  NewsDetailsVC.swift
//  HelloFolk
//
//  Created by Issam on 3/19/21.
//

import UIKit

class NewsDetailsVC: BaseHelloFolkVC {
    @IBOutlet weak var tableview: UITableView!
    var data : PostM = PostM()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.accessibilityIdentifier = Constants.NewsDetailsVC
    }
    override func setupViews() {
        super.setupViews()
        init_tableview()
        fetchData()
    }
    override func fetchData() {
        super.fetchData()
        
        self.showLoading()
        RS.shared.newsDetailsVM.fetchData().subscribe(onNext: { res in
            self.data = res
            self.tableview.reloadData()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) { // for autoHieght
                self.tableview.reloadData()
            }
        },onError: { (error) in
            //the error has been resolved on VM
        }, onCompleted: {
            self.hideLoading()
        }).disposed(by: disposeBag)
    }
}

extension NewsDetailsVC : UITableViewDelegate, UITableViewDataSource {
    func init_tableview(){
        tableview.register(UINib(nibName: Constants.NewsCell, bundle: Bundle.main), forCellReuseIdentifier: Constants.NewsCell)
        tableview.register(UINib(nibName: Constants.NewsDetailsCell, bundle: Bundle.main), forCellReuseIdentifier: Constants.NewsDetailsCell)
        tableview.delegate = self
        tableview.dataSource = self
        tableview.rowHeight = UITableView.automaticDimension
        tableview.estimatedRowHeight = UITableView.automaticDimension

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.data.title == nil && self.data.body == nil{
            return 0
        }
        return (self.data.comments ?? []).count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{ // post
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.NewsCell) as! NewsCell
            cell.data = self.data
            return cell
        }else{ // comments
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.NewsDetailsCell) as! NewsDetailsCell
            cell.data = self.data.comments?[indexPath.row - 1]
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
