//
//  HomeVC.swift
//  HelloFolk
//
//  Created by Issam on 3/18/21.
//

import UIKit

class HomeVC: BaseHelloFolkVC {
    @IBOutlet weak var tableview: UITableView!
    var arrayData : postsM = []
    
    override func viewDidLoad() {
        Provider.shared.baseNavBarVC = self
        super.viewDidLoad()
        view.accessibilityIdentifier = Constants.HomeVC
    }
    override func setupViews() {
        super.setupViews()
        init_tableview()
        fetchData()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
         .lightContent
    }
    override func fetchData() {
        super.fetchData()
        self.showLoading()
        RS.shared.newsVM.fetchData().subscribe(onNext: { res in
            self.arrayData = res
            self.tableview.reloadData()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) { // for autoHieght
                self.tableview.reloadData()
            }
        },onError: { (error) in
            //the error has been resolved on VM
        }, onCompleted: {
            self.hideLoading()
        }).disposed(by: disposeBag)
    }
}
    
extension HomeVC : UITableViewDelegate, UITableViewDataSource {
    func init_tableview(){
        tableview.register(UINib(nibName: Constants.NewsCell, bundle: Bundle.main), forCellReuseIdentifier: Constants.NewsCell)
        tableview.delegate = self
        tableview.dataSource = self
        tableview.rowHeight = UITableView.automaticDimension
        tableview.estimatedRowHeight = UITableView.automaticDimension

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.arrayData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.NewsCell) as! NewsCell
        cell.data = self.arrayData[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: Constants.StoryBoard.Main.rawValue, bundle: Bundle.main)
        let VC = storyboard.instantiateViewController(withIdentifier: Constants.NewsDetailsVC)
        RS.shared.newsDetailsVM.data = self.arrayData[indexPath.row]
        navigationController?.pushViewController(VC, animated: true)
    }
}
