//
//  BaseVC.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//
import UIKit
import SideMenu
import SDWebImage
import WXImageCompress
import SwiftyJSON
import Lottie
import RxSwift
import RxCocoa

class BaseVC: UIViewController {
    var hudSuperV : UIView!
    var ref = UIRefreshControl()
    var tap : UITapGestureRecognizer!
    var errorResult : Driver<ErrorResult> = Driver.never()
    var progressView: AnimationView = .init(name: Constants.Loading)
    var isfetchData = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupStyle()
        self.ref.addTarget(self, action: #selector(fetchData), for: .valueChanged)
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.font : ThemeApp.Fonts.semiBold20,
             NSAttributedString.Key.foregroundColor: ThemeApp.Colors.title]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        unregisterReceivers()
        registerReceivers()
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        unregisterReceivers()
    }
    
    
}
