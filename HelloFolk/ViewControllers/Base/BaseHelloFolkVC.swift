//
//  BaseHelloFolkVC.swift
//  HelloFolk
//
//  Created by Issam on 3/17/21.
//

import Foundation
import UIKit
import Then
class BaseHelloFolkVC : BaseVC {
    
    var navBarView : UIView?
    var leftbtnBarView : UIView?
    var rightbtnBarView : UIView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if (self.navigationController?.viewControllers.count ?? 0) != 1 {
            setupBackBtn()
        }

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        leftbtnBarView?.removeFromSuperview()
        rightbtnBarView?.removeFromSuperview()
    }
    override func setupLeftBtn(){
        if navBarView == nil { return }
        let btn = UIButton().then{
            leftbtnBarView = $0
            $0.translatesAutoresizingMaskIntoConstraints = false
            navBarView!.addSubview($0)
            $0.centerYAnchor.constraint(equalTo: navBarView!.centerYAnchor, constant: 0).isActive = true
            $0.leadingAnchor.constraint(equalTo: navBarView!.leadingAnchor, constant: 8).isActive = true
            $0.heightAnchor.constraint(equalToConstant: 50).isActive = true
            $0.widthAnchor.constraint(equalToConstant: 50).isActive = true
        }

        _ = UIImageView().then{
            $0.image = UIImage(named:  Constants.MenuImg)
            $0.translatesAutoresizingMaskIntoConstraints = false
            btn.addSubview($0)
            $0.centerYAnchor.constraint(equalTo: btn.centerYAnchor, constant: 0).isActive = true
            $0.centerXAnchor.constraint(equalTo: btn.centerXAnchor, constant: 0).isActive = true
            $0.heightAnchor.constraint(equalToConstant: 19).isActive = true
            $0.widthAnchor.constraint(equalToConstant: 25).isActive = true
        }
        
    }

    override func setupRightBtn(selector: Selector, img: String? = nil,btnTitle : String? = nil, tint: UIColor = .black){
        if navBarView == nil { return }
        let btn = UIButton()
        rightbtnBarView = btn

        btn.translatesAutoresizingMaskIntoConstraints = false
        navBarView!.addSubview(btn)
        if img != nil{
            let image = UIImageView()
            image.image = UIImage(named: img ?? "")
            image.contentMode = .scaleAspectFit
            image.translatesAutoresizingMaskIntoConstraints = false
            btn.addSubview(image)
            image.centerYAnchor.constraint(equalTo: btn.centerYAnchor, constant: 0).isActive = true
            image.centerXAnchor.constraint(equalTo: btn.centerXAnchor, constant: 0).isActive = true
            image.heightAnchor.constraint(equalToConstant: 19).isActive = true
            image.widthAnchor.constraint(equalToConstant: 25).isActive = true
        }
        if btnTitle != nil {
            btn.setTitle(btnTitle ?? "", for: .normal)
        }
        btn.centerYAnchor.constraint(equalTo: navBarView!.centerYAnchor, constant: 0).isActive = true
        btn.trailingAnchor.constraint(equalTo: navBarView!.trailingAnchor, constant: -8).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        btn.widthAnchor.constraint(equalToConstant: 50).isActive = true
        btn.addTarget(self, action: selector, for: .touchUpInside)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
         .default
    }

    
}
