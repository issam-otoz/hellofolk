//
//  UserM.swift
//  HelloFolk
//
//  Created by Issam on 3/18/21.
//

import Foundation

// MARK: - UserM
struct userM: Codable {
    let id: Int?
    let name, username, email: String?
    let address: address?
    let phone, website: String?
    let company: company?
}

// MARK: - Address
struct address: Codable {
    let street, suite, city, zipcode: String?
    let geo: geo?
}

// MARK: - Geo
struct geo: Codable {
    let lat, lng: String?
}

// MARK: - Company
struct company: Codable {
    let name, catchPhrase, bs: String?
}

typealias usersM = [userM]
