//
//  UsersM.swift
//  HelloFolk
//
//  Created by Issam on 3/18/21.
//

import Foundation

// MARK: - PostM
struct PostM: Codable  {
    var userID, id: Int?
    var title, body: String?
    var user : userM?
    var comments : CommentsM?
    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case id, title, body
    }
}

typealias postsM = [PostM]
