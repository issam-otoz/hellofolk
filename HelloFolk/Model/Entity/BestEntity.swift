//
//  BestEntity.swift
//  HelloFolk
//
//  Created by Issam on 3/17/21.
//

import Foundation
import ObjectMapper
import SwiftyJSON

class BaseEntity : Mappable{
    
    var created_at : String!
    var updated_at : String!
    var deleted_at : String!
    
    public func mapping(map: Map) {
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        deleted_at <- map["deleted_at"]
    }
    
    public required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    
    let transformInt = TransformOf<Int, String>(fromJSON: { (value: String?) -> Int? in
        // transform value from String? to Int?
        return Int(value!)
    }, toJSON: { (value: Int?) -> String? in
        // transform value from Int? to String?
        if let value = value {
            return String(value)
        }
        return nil
    })
    
    let transformDouble = TransformOf<Double, String>(fromJSON: { (value: String?) -> Double? in
        // transform value from String? to Int?
        return Double(value!)
    }, toJSON: { (value: Double?) -> String? in
        // transform value from Int? to String?
        if let value = value {
            return String(value)
        }
        return nil
    })
    
    
}
