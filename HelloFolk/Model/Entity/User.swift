//
//  User.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import UIKit
import SwiftyJSON

class User: NSObject,NSCoding {
    static let KEY_USER_ME : String = "me"
    
    //Auth
    var ID : Int!
    var Token : String!
    var MSTToken : String!
    var isDark: Bool!
    var fontSizeId : Int!
    var mobilelangue : String!

    var posts : postsM!
    var usersPosts : usersM!
    
    //MARK: chache current user
    static func getCurrentUser() -> User
    {
        if let archivedData = UserDefaults.standard.object(forKey: KEY_USER_ME) as? Data
        {
            do {
                let me: User = try (NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(archivedData) as? User)!
            //(with: archivedData) as? UserE)!
            return me
            }catch{
            return User()
            }
            
        }
        return User()
    }
    override init(){
        
    }
    
    static func saveMe(me : User)
    {
        let archivedObject = NSKeyedArchiver.archivedData(withRootObject: me)
        UserDefaults.standard.set(archivedObject, forKey: KEY_USER_ME)
        UserDefaults.standard.synchronize()
    }
    static func logout(){
        RS.ClearMe()
        var me = User.getCurrentUser();
        me = User()
        User.saveMe(me: me);
    }
    
    static func clearMe()
    {
        UserDefaults.standard.removeObject(forKey: KEY_USER_ME)
    }
    
    func encode(with coder: NSCoder) {
        
        //Auth
        coder.encode(ID, forKey: "ID")
        coder.encode(Token, forKey: "Token")
        coder.encode(MSTToken, forKey: "MSTToken")
        coder.encode(isDark, forKey: "isDark")
        coder.encode(fontSizeId, forKey: "fontSizeId")
        coder.encode(mobilelangue, forKey: "mobilelangue")
        
        let en_posts = try? JSONEncoder().encode(posts)
        coder.encode(en_posts, forKey: "posts")
        let en_usersPosts = try? JSONEncoder().encode(usersPosts)
        coder.encode(en_usersPosts, forKey: "usersPosts")
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        self.ID = aDecoder.decodeObject(forKey: "ID") as? Int
        self.Token = aDecoder.decodeObject(forKey: "Token")  as? String
        self.MSTToken = aDecoder.decodeObject(forKey: "MSTToken")  as? String
        self.isDark = aDecoder.decodeObject(forKey: "isDark") as? Bool
        self.fontSizeId = aDecoder.decodeObject(forKey: "fontSizeId") as? Int
        self.mobilelangue = aDecoder.decodeObject(forKey: "mobilelangue") as? String
        let date_posts = aDecoder.decodeObject(forKey: "posts")
        self.posts = try? JSONDecoder().decode(postsM.self, from:JSON(date_posts).rawData())
        let date_usersPosts = aDecoder.decodeObject(forKey: "usersPosts")
        self.usersPosts = try? JSONDecoder().decode(usersM.self, from:JSON(date_usersPosts).rawData())
        
    }
    static func getObject(_ dic : [String : Any]) -> User{
        let me = User.getCurrentUser()
        
        me.ID =  dic["ID"] as? Int
        me.Token =  dic["Token"] as? String
        me.MSTToken =  dic["MSTToken"] as? String
        me.fontSizeId =  dic["fontSizeId"] as? Int
        me.mobilelangue =  dic["mobilelangue"] as? String

        return me
    }
    
}

