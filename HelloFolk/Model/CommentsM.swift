//
//  CommentsM.swift
//  HelloFolk
//
//  Created by Issam on 3/18/21.
//

import Foundation

// MARK: - CommentsMElement
struct CommentM: Codable {
    let postID, id: Int?
    let name, email, body: String?

    enum CodingKeys: String, CodingKey {
        case postID = "postId"
        case id, name, email, body
    }
}

typealias CommentsM = [CommentM]
