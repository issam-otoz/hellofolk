//
//  CardView.swift
//  HelloFolk
//
//  Created by Issam on 3/19/21.
//

import Foundation
import UIKit

//@IBDesignable
class CardView: UIView {

    @IBInspectable var shadowColor2: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity2: Float = 0.2//0.5
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor2?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity2
        layer.shadowPath = shadowPath.cgPath
    }
}

//@IBDesignable
class ButtonCardView: UIButton {
    
    @IBInspectable var shadowColor2: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity2: Float = 0.2//0.5
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor2?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity2
        layer.shadowPath = shadowPath.cgPath
    }
    
    
}

extension UIView{

    func addCardView(){
        let shadowOffsetWidth: Int = 0
        let shadowOffsetHeight: Int = Int(0.2)
        let shadowColor2: UIColor? = UIColor.black
        let shadowOpacity2: Float = 0.2//0.5
        layer.cornerRadius = cornerRadius
        
        let shadowPath = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height - 4), cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor2?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity2
        layer.shadowPath = shadowPath.cgPath
    }
}
