//
//  HelloFolkLabel.swift
//  HelloFolk
//
//  Created by Issam on 3/17/21.
//

import Foundation
import UIKit

class HelloFolkBLabel : UILabel {

    @IBInspectable var fontSize: CGFloat = 22.0
    @IBInspectable var fontFamily: String = "Regular"
    @IBInspectable var txtColor: UIColor? = UIColor.black
    @IBInspectable var alignment: String = ""
    @IBInspectable var textValue: String = ""
   
    override func layoutSubviews() {
        self.textColor = txtColor
        self.attributedText = NSMutableAttributedString(string: textValue, attributes: [NSAttributedString.Key.kern: 0.23])
        self.numberOfLines = 0
        self.lineBreakMode = .byWordWrapping
        helloFolkFontFamily()
        helloFolkTextAlignment()
    }
    func helloFolkFontFamily(){
        var fontName = fontFamily
        switch fontFamily {
        case "Bold":
            fontName = HelloFolkFont.Bold.rawValue
        case "SemiBold":
            fontName = HelloFolkFont.SemiBold.rawValue
        case "Regular":
            fontName = HelloFolkFont.Regular.rawValue
        case "Light":
            fontName = HelloFolkFont.Light.rawValue
        default:
            fontName = HelloFolkFont.Regular.rawValue
        }
        self.font = UIFont(name: fontName, size: fontSize)!
    }
    func helloFolkTextAlignment(){
        switch alignment {
        case "Center":
            self.textAlignment = .center
        case "Left":
            self.textAlignment = .left
        case "Right":
            self.textAlignment = .right
        case "Justified":
            self.textAlignment = .justified
        case "natural":
            self.textAlignment = .natural
        default:
            return
        }
    }
}
enum HelloFolkFont : String, Codable {
    case Bold = "Noah-Bold"
    case SemiBold = "Noah-BoldItalic"
    case Regular = "Noah-Regular"
//    case Medium = "Noah Regular 400"
    case Light = "Noah-RegularItalic"
}
enum HelloFolkTextAlignment : String, Codable {
    case Bold = "Noah-Bold"
    case SemiBold = "Noah-BoldItalic"
    case Regular = "Noah-Regular"
//    case Medium = "Noah Regular 400"
    case Light = "Noah-RegularItalic"
}
