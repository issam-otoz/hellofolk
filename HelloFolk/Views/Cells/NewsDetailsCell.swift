//
//  NewsDetailsVC.swift
//  HelloFolk
//
//  Created by Issam on 3/19/21.
//

import UIKit

class NewsDetailsCell: UITableViewCell {

    var data : CommentM? {
        didSet{
            setupView()
        }
    }
    @IBOutlet weak var userNamelbl: HelloFolkBLabel!
    @IBOutlet weak var bodylbl: HelloFolkBLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setupView(){
        if data == nil {return}
        userNamelbl.textValue = data!.name ?? "Anonymous"
        bodylbl.textValue = data!.body ?? "this Article without body"
    }
    
}
