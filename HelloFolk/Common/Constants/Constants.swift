//
//  Constants.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import Foundation
import UIKit
struct Constants {
    
    //appDelegate
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    enum StoryBoard : String{
        case Authenticate = "Authenticate"
        case Onboarding = "Onboarding"
        case Main = "Main"
    }
    
    //Lorem
    static let TitleLorem = "share your news\n with world "
    static let BodyLorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Velit vulputate ornare magna integer. Ullamcorper quam fermentum, in pellentesque. Velit magna luctus semper curabitur malesuada eget. Vel sit neque, tristique sit."
    // text
    static let LbOk = "Ok"
    static let LbCancel = "Cancel"
    static let PleaseWait = "Please Wait..."
    static let Search = "Search"
    static let Tag = "tag"
    static let TryAgain = "Try Again"
    static let Cancel = "Cancel"
    static let Loading = "loading"
    
    // images
    static let LogoImg = "logo"
    static let MenuImg = "Menu"
    static let BackImg = "Back"
    static let Splash0 = "Splash0"
    static let Splash1 = "Splash1"
    static let Splash2 = "Splash2"
    static let Splash3 = "Splash3"
    
    //lottie
    static let RdhvLottie = "rdhvjhkp"
    static let NhvmLottie = "nhvmluce"
    
    //fonts
    static let PoppinsRegular = "Poppins-Regular"
    static let PoppinsMedium = "Poppins-Medium"
    static let PoppinsSemiBold =  "Poppins-SemiBold"
    static let CoconNextArabicBold =  "CoconNextArabic-Bold"
    static let CoconNextArabicLight =  "CoconNextArabic-Light"
    
    
    //VC Names
    static let NewsDetailsVC = "NewsDetailsVC"
    static let SideMenuVC = "SideMenuVC"
    static let CoverVC = "CoverVC"
    static let NavHomeVC = "NavHome"
    static let HomeVC = "HomeVC"
    static let OnBoardingSplashVC = "OnBoardingSplashVC"
    static let OnBoardingSplash1VC = "OnBoardingSplash1VC"
    static let OnBoardingSplash2VC = "OnBoardingSplash2VC"
    static let SplashVC = "SplashVC"

    //Cells
    static let NewsCell = "NewsCell"
    static let NewsDetailsCell = "NewsDetailsCell"
    
}
enum LanguageName: String, CodingKey {
    case Arabic = "ar"
    case English = "en"
}
