//
//  DisplayAlert.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

import Foundation
import UIKit
import SwiftMessages
class DisplayAlert {
    static var shared : DisplayAlert = DisplayAlert()
    static func show(title:String = "",
                     message:String,
                     okTitle:String = Constants.LbOk,
                     cancelTitle:String? = nil,
                     okHandler:((UIAlertAction) -> Swift.Void)? = nil,
                     cancelHandler:((UIAlertAction) -> Swift.Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if let title = cancelTitle, title.count > 0 {
            let btnCancel = UIAlertAction(title: title, style: .cancel, handler: cancelHandler)
            alert.addAction(btnCancel)
        }
        
        let btnOk = UIAlertAction(title: okTitle, style: .default, handler: okHandler)
        alert.addAction(btnOk);
        
        alert.show(animated: true)
    }
    
    static func showMultipleChoices(choices : [String], controller: UIViewController, callback : @escaping (String) -> Void){
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        for choice in choices {
            let action = UIAlertAction(title: choice, style: .default) { (ok) in
                callback(choice)
            }
            alert.addAction(action)
        }
        if let popoverPresentationController = alert.popoverPresentationController {
            popoverPresentationController.sourceView = controller.view
            //popoverPresentationController.sourceRect = sender.bounds
        }
        controller.present(alert, animated: true, completion: nil)
    }

}

extension UIViewController{
    open func showMessage(title: String = "", message: String, duration: Double = 2.0, theme: Theme = UISettings.defaultMessageTheme, didShow: (() -> Void)? = nil, didHide: (() -> Void)? = nil)
       {
           if !self.shouldShowMessages
           {
               NSLog("Message with title: \(title) and text: \(message) Display Skipped")
               return
           }
           let view = MessageView.viewFromNib(layout: .cardView)
           
           // Theme message elements with the warning style.
           view.configureTheme(theme)
           
           // Add a drop shadow.
           view.configureDropShadow()
           
           // Set message title, body, and icon. Here, we're overriding the default warning
           // image with an emoji character.
           view.configureContent(title: title, body: message, iconText: "")

           // Increase the external margin around the card. In general, the effect of this setting
           // depends on how the given layout is constrained to the layout margins.
           view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
           
           // Reduce the corner radius (applicable to layouts featuring rounded corners).
           let roundingView = (view.backgroundView)
           
           roundingView?.cornerRadius = 10
           
           
           var config = SwiftMessages.Config()
           
           // Slide up from the bottom.
           config.presentationStyle = .bottom
           
           // Display in a window at the specified window level: UIWindow.Level.statusBar
           // displays over the status bar while UIWindow.Level.normal displays under.
           //        config.presentationContext = .window(windowLevel: .statusBar)
           
           // Disable the default auto-hiding behavior.
           if duration == 0
           {
               config.duration = .forever

           }else{
               config.duration = .seconds(seconds: duration)

           }
           
           // Dim the background like a popover view. Hide when the background is tapped.
           config.dimMode = .gray(interactive: true)
           
           // Disable the interactive pan-to-hide gesture.
           config.interactiveHide = false
           
           // Specify a status bar style to if the message is displayed directly under the status bar.
           //        config.preferredStatusBarStyle = .lightContent
           //
           // Specify one or more event listeners to respond to show and hide events.
           config.eventListeners.append() { event in
               if case .didShow = event {
                   didShow?()
               }
               if case .didHide = event {
                   didHide?()
               }
           }
           view.button?.isHidden = true
           
           SwiftMessages.show(config: config, view: view)
           
       }
}
