//
//  OpenUrl.swift
//  HelloFolk
//
//  Created by Issam on 3/16/21.
//

//refrance
//https://stackoverflow.com/questions/51759148/download-pdf-and-save-to-the-files-in-iphone-not-to-the-app-data-swift
//https://stackoverflow.com/questions/45869306/how-can-i-open-a-file-out-of-memory-in-a-uidocumentinteractioncontroller
import Foundation
import UIKit
import QuickLook

class OpenUrl{
    static var shared = OpenUrl()
    static func show(customURL : String){
        Print("URL is \(customURL)")

        if customURL == ""{
            DisplayAlert.show(title: "", message: "Error in URL \(customURL)")
        }
    //        let encodedLink = customURL.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        if let encodedURL : URL = NSURL(string: customURL) as URL?{
            let objectsToShare:URL = encodedURL
            Print("URL is encodedURL\(encodedURL)")
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(objectsToShare)
            } else {
                UIApplication.shared.openURL(objectsToShare)
            }
        }else{
            DisplayAlert.show(title: "", message: "Error in URL \(customURL)")
        }
    }
    
    func download(VC : BaseVC? = nil,urlString : String,localFileUrl: @escaping (URL) -> Void){
    //        let urlString = "your file url"
        let url = URL(string: urlString)
        let fileName = String((url!.lastPathComponent)) as NSString
        // Create destination URL
    //        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first)!
        let documentsUrl:URL =  (FileManager.default.temporaryDirectory)
        let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")
        //Create URL to the source file you want to download
        let fileURL = URL(string: urlString)
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = URLRequest(url:fileURL!)
        DispatchQueue.main.async {
            guard let vc = VC else {return }
            vc.showLoading()
        }
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            DispatchQueue.main.async {
                guard let vc = VC else {return }
                vc.hideLoading()
            }
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    Print("Successfully downloaded. Status code: \(statusCode)")
                }
                do {
                    if FileManager.default.fileExists(atPath: destinationFileUrl.path) {
                        try! FileManager.default.removeItem(at: destinationFileUrl)
                    }
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                        localFileUrl(destinationFileUrl)
                } catch (let writeError) {
                    Print("Error creating a file \(destinationFileUrl) : \(writeError)")
                }
            } else {
                Print("Error took place while downloading a file. Error description: \(error?.localizedDescription ?? "")")
            }
        }
        task.resume()
    }
    
//    static func downloadAndPreview(VC : UIViewController,expFile : ExpFile){
//        OpenUrl.shared.download(VC: VC, urlString: expFile.getUrl()) { (res) in
//            DispatchQueue.main.async {
//                shareURL(VC: VC, url: res , title: expFile.getName())
//            }
//        }
//    }
    static func shareURL(VC : UIViewController,url: URL , title : String){
            let dc = UIDocumentInteractionController(url: url)
            dc.url = url
            dc.name = title
            dc.delegate = VC as? UIDocumentInteractionControllerDelegate
            dc.presentPreview(animated: true)

    }
    func generateThumbnail(urlString : String,width : CGFloat,height : CGFloat,completion: @escaping (UIImage) -> Void){
        if let url = RS.shared.onlineToLocalUrl[urlString] {
            self.ThumbnailGenerator(width: width, height: height, fileAt: url) { (res) in
                completion(res)
            }
            return
        }
        OpenUrl.shared.download(urlString: urlString) { (res) in
            RS.shared.onlineToLocalUrl[urlString] = res
            self.ThumbnailGenerator(width: width, height: height, fileAt: res) { (res) in
                completion(res)
            }
        }
    }
    func ThumbnailGenerator(width : CGFloat = 165,height : CGFloat = 102,fileAt : URL,completion: @escaping (UIImage) -> Void){
        DispatchQueue.main.async {
            let size = CGSize(width: width, height: height)
            let scale = UIScreen.main.scale
            if #available(iOS 13.0, *) {
                let request = QLThumbnailGenerator.Request(fileAt: fileAt, size: size, scale: scale, representationTypes: .all)
                let generator = QLThumbnailGenerator.shared
                generator.generateRepresentations(for: request) { thumbnail, _, error in
                    if let thumbnail = thumbnail {
                        completion(thumbnail.uiImage)
                    } else if let error = error {
                      // Handle error
                        Print(error)
                    }
                }
            }
        }
    }
}
