//
//  HelloFolkTests.swift
//  HelloFolkTests
//
//  Created by Issam on 3/16/21.
//

import XCTest
//import RxBlocking
import RxTest
import RxSwift

@testable import HelloFolk

class HelloFolkTests: XCTestCase {
    
    var scheduler: TestScheduler!
    var disposeBag: DisposeBag!


    override func setUpWithError() throws {
        scheduler = TestScheduler(initialClock: 0)
        disposeBag = DisposeBag()
    }

    override func tearDownWithError() throws {
    }

    func testGetUsers() throws {
        let e = expectation(description: "Alamofire GetUsers")
        e.assertForOverFulfill = false
        let rx = Communication.shared.getUsers()

        rx
          .subscribe(onNext: { res in
            switch res{
            case .success(let res):
                if !res.isEmpty{
                    for i in res{
                        XCTAssert(i.id != nil)
                        XCTAssert(i.name != nil)
                        XCTAssert(i.username != nil)
                        XCTAssert(i.phone != nil)
                    }
                }
                e.fulfill()
                break
            case .failure(let error):
                break
            }
          },onError: { (error) in
          },onCompleted: {
          })
          .disposed(by: disposeBag)
        waitForExpectations(timeout: 10.0, handler: nil)

    }
    
    func testGetPosts() throws {
        let e = expectation(description: "Alamofire getPosts")
        e.assertForOverFulfill = false

        let rx = Communication.shared.getPosts()
        rx
          .subscribe(onNext: { res in
            switch res{
            case .success(let res):
                if !res.isEmpty{
                    for i in res{
                        XCTAssert(i.id != nil)
                        XCTAssert(i.userID != nil)
                        XCTAssert(i.title != nil)
                        XCTAssert(i.body != nil)
                    }
                }
                e.fulfill()
                break
            case .failure(let error):
                break
            }
          },onError: { (error) in
          },onCompleted: {
          })
          .disposed(by: disposeBag)
        waitForExpectations(timeout: 10.0, handler: nil)

    }
    
    func testGetCommentsPost() throws {
        let e1 = expectation(description: "Alamofire GetPost to get Comments")
        let e2 = expectation(description: "Alamofire GetCommentsPost")
        e1.assertForOverFulfill = false
        e2.assertForOverFulfill = false

        let rx = Communication.shared.getPosts()
        rx
          .subscribe(onNext: { res in
            switch res{
            case .success(let res):
                if !res.isEmpty{
                    
                    let firstItemInRange = res.first
                    let lastItemInRange = res.last
                    let midItemInRange = res[res.count/2]
                    let indexItemBeforeMideInRange = res.count/2 - 1
                    let indexItemAfterMideInRange = res.count/2 + 1
                    
                    try? self.testCommentsPost(data: firstItemInRange, e2)
                    try? self.testCommentsPost(data: lastItemInRange, e2)
                    try? self.testCommentsPost(data: midItemInRange, e2)
                    
                    if indexItemBeforeMideInRange > 0 {
                        try? self.testCommentsPost(data: res[indexItemBeforeMideInRange], e2)
                    }
                    if indexItemAfterMideInRange < res.count {
                        try? self.testCommentsPost(data: res[indexItemAfterMideInRange], e2)
                    }
                }
                e1.fulfill()
                break
            case .failure(let error):
                break
            }
          },onError: { (error) in
          },onCompleted: {
          })
          .disposed(by: disposeBag)
        wait(for: [e1,e2], timeout: 12)
    }
    func testCommentsPost(data : PostM? ,_  e : XCTestExpectation) throws {
        let rx = Communication.shared.getCommentsPost(postId: data?.id ?? -1)
        rx
          .subscribe(onNext: { res in
            switch res{
            case .success(let res):
                if !res.isEmpty{
                    for i in res{
                        XCTAssert(i.id != nil)
                        XCTAssert(i.name != nil)
                        XCTAssert(i.email != nil)
                        XCTAssert(i.body != nil)
                    }
                }
                e.fulfill()
                break
            case .failure(let error):
                break
            }
          },onError: { (error) in
          },onCompleted: {
          })
          .disposed(by: disposeBag)
    }
    
    func testNewsVM() throws {
        let e = expectation(description: "test NewsVM")
        e.assertForOverFulfill = false
        
        let t = NewsVM()
        let rx = t.fetchData()
        rx
          .subscribe(onNext: { res in
                XCTAssertTrue(!t.posts.isEmpty)
                XCTAssertTrue(!t.users.isEmpty)
                e.fulfill()
          },onError: { (error) in
          },onCompleted: {
          })
          .disposed(by: disposeBag)
        wait(for: [e], timeout: 10)

    }
    
    func testGetNewsDetailsVM() throws {
        let e1 = expectation(description: "test NewsVM")
        let e2 = expectation(description: "test NewsDetailsVM")
        e1.assertForOverFulfill = false
        e2.assertForOverFulfill = false
        
        let t = NewsVM()
        let rx = t.fetchData()
        rx
          .subscribe(onNext: { res in
            if !t.posts.isEmpty{
                
                let firstItemInRange = t.posts.first
                let lastItemInRange = t.posts.last
                let midItemInRange = t.posts[t.posts.count/2]
                let indexItemBeforeMideInRange = t.posts.count/2 - 1
                let indexItemAfterMideInRange = t.posts.count/2 + 1
                try? self.testNewsDetailsVM(data: firstItemInRange, e2)
                try? self.testNewsDetailsVM(data: lastItemInRange, e2)
                try? self.testNewsDetailsVM(data: midItemInRange, e2)
                
                if indexItemBeforeMideInRange > 0 {
                    try? self.testNewsDetailsVM(data: t.posts[indexItemBeforeMideInRange], e2)
                }
                if indexItemAfterMideInRange < t.posts.count {
                    try? self.testNewsDetailsVM(data: t.posts[indexItemAfterMideInRange], e2)
                }

            }
            e1.fulfill()
          },onError: { (error) in
          },onCompleted: {
          })
          .disposed(by: disposeBag)
        wait(for: [e1,e2], timeout: 12)
        
    }
    func testNewsDetailsVM(data : PostM? ,_  e : XCTestExpectation) throws {
        
        let t = NewsDetailsVM()
        let rx = t.fetchData()
        rx
          .subscribe(onNext: { res in
            if !(t.data.comments ?? []).isEmpty{
                XCTAssertTrue(!t.data.comments!.isEmpty)
                XCTAssertTrue(t.data.title!.isNotEmpty)
                XCTAssertTrue(t.data.body!.isNotEmpty)
            }
            e.fulfill()
          },onError: { (error) in
          },onCompleted: {
          })
          .disposed(by: disposeBag)
//        wait(for: [e], timeout: 10)
        
    }
    
    func testMockData() throws {
        let d1 = Communication.shared.loadJson(filename: "users", objectType: usersM.self)
        XCTAssertTrue(!d1!.isEmpty)
        let d2 = Communication.shared.loadJson(filename: "posts", objectType: postsM.self)
        XCTAssertTrue(!d2!.isEmpty)
        let d3 = Communication.shared.loadJson(filename: "comments", objectType: CommentsM.self)
        XCTAssertTrue(!d3!.isEmpty)

    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
