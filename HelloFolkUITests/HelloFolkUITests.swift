//
//  HelloFolkUITests.swift
//  HelloFolkUITests
//
//  Created by Issam on 3/16/21.
//

import XCTest
@testable import HelloFolk

class HelloFolkUITests: XCTestCase {
    
    var app: XCUIApplication!

    override func setUpWithError() throws {
        // Since UI tests are more expensive to run, it's usually a good idea
        // to exit if a failure was encountered
        continueAfterFailure = false

        app = XCUIApplication()

        // We send a command line argument to our app,
        // to enable it to reset its state
        app.launchArguments.append("--uitesting")
        
        // to enable Mock Data
        app.launchArguments.append("--MOCKFLAG")

    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        
        app.launch()

        // Make sure we're displaying onboarding
        XCTAssertTrue(app.isDisplayingSplash)
        wait(for: 5) //wait the animation of splash screen to finish
        XCTAssertTrue(app.isDisplayingOnboarding1)
        app.buttons["Next Vector"].tap()
        XCTAssertTrue(app.isDisplayingOnboarding2)
        app.buttons["Back Vector"].tap()
        XCTAssertTrue(app.isDisplayingOnboarding1)
        app.buttons["Next Vector"].tap()
        XCTAssertTrue(app.isDisplayingOnboarding2)
        app.buttons["Next Vector"].tap()
        XCTAssertTrue(app.isDisplayingHome)
        
        let n = app.tables.cells.count

        if n > 0 {
            XCTAssertTrue(app.isDisplayingHome)
            app.tables.cells.element(boundBy: Int(round(Double(n-1)))).tap()
            XCTAssertTrue(app.isDisplayingNewsDetails)
            app.swipeUp(velocity: .fast)
            app.buttons["Back"].tap()
        
            XCTAssertTrue(app.isDisplayingHome)
            app.tables.cells.element(boundBy: Int(round(Double(n-1)))).tap()
            XCTAssertTrue(app.isDisplayingNewsDetails)
            app.swipeUp(velocity: .fast)
            app.buttons["Back"].tap()
            
        
            XCTAssertTrue(app.isDisplayingHome)
            app.tables.cells.element(boundBy: Int(round(Double(n-1)))).tap()
            XCTAssertTrue(app.isDisplayingNewsDetails)
            app.swipeUp(velocity: .fast)
            app.buttons["Back"].tap()
            
            XCTAssertTrue(app.isDisplayingHome)
            app.tables.cells.element(boundBy: Int(round(Double(n-1)))).tap()
            XCTAssertTrue(app.isDisplayingNewsDetails)
            app.swipeUp(velocity: .fast)
            app.buttons["Back"].tap()
 
        }

        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
extension XCTestCase {

  func wait(for duration: TimeInterval) {
    let waitExpectation = expectation(description: "Waiting")

    let when = DispatchTime.now() + duration
    DispatchQueue.main.asyncAfter(deadline: when) {
      waitExpectation.fulfill()
    }

    // We use a buffer here to avoid flakiness with Timer on CI
    waitForExpectations(timeout: duration + 0.5)
  }
}
extension XCUIApplication {
    var isDisplayingSplash: Bool {
        return otherElements["SplashVC"].exists
    }
    var isDisplayingOnboarding1: Bool {
        return otherElements["OnBoardingSplash1VC"].exists
    }
    var isDisplayingOnboarding2: Bool {
        return otherElements["OnBoardingSplash2VC"].exists
    }
    var isDisplayingHome: Bool {
        return otherElements["HomeVC"].exists
    }
    var isDisplayingNewsDetails: Bool {
        return otherElements["NewsDetailsVC"].exists
    }
}
